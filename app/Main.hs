{-# LANGUAGE OverloadedStrings #-}
module Main where

import Api
import Network.Wai.Handler.Warp
import Database.Persist
import Database.Persist.Sqlite
import qualified Data.UUID as UUID
import Models
import UserUUID
import Control.Monad.IO.Class (liftIO)

main :: IO ()
main = do
  myApp <- mkApp ":memory:"

  liftIO $ run 8081 myApp
