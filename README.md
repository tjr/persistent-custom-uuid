# persistent-custom-uuid

`UserUUID` type and instances in [src/UserUUID.hs](./src/UserUUID.hs)

`User` and `Profile` models defined in [src/Models.hs](./src/Models.hs)

Servant api defined in [src/Api.hs](./src/Api.hs)
