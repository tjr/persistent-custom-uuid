{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module UserUUID where
import           Data.Aeson
import qualified Data.ByteString.Char8 as B8
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Text.Encoding as E
import           Data.UUID (UUID)
import qualified Data.UUID as UUID
import Database.Persist
import Database.Persist.Sql
import           Database.Persist.TH
import           Web.HttpApiData
import           Web.PathPieces

derivePersistField "UUID"

newtype UserUUID = UserUUID {unUserUUID :: UUID}
  deriving (Read, Show, Eq, Ord, ToJSON, FromJSON, PersistField, PersistFieldSql, ToHttpApiData, FromHttpApiData)

instance PathPiece UserUUID where
  fromPathPiece u = UserUUID <$> (UUID.fromString . B8.unpack . E.encodeUtf8 $ u)
  toPathPiece (UserUUID u) = E.decodeUtf8 . B8.pack . UUID.toString $ u
