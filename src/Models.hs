{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Models where
import Data.UUID (UUID)
import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH

import UserUUID (UserUUID)

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
User json
  uuid UserUUID
  Primary uuid
  deriving Show

Profile json
  ownerFk UserId
  Primary ownerFk
|]
