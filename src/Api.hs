{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Api where

import Servant
import Servant.API
import Models
import Database.Persist
import Database.Persist.Sql
import Database.Persist.Sqlite
import Database.Persist.TH
import Control.Monad.Logger
import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.IO.Unlift
import Data.Text (Text)
import Conduit
import qualified Data.UUID as UUID
import UserUUID
import           Data.String.Conversions (cs)


type UserAPI = "users" :> Get '[JSON] [User]
  :<|> "profiles" :> Get '[JSON] [Profile]

userUUID = UserUUID UUID.nil


getUsers :: ConnectionPool -> IO [User]
getUsers pool = flip runSqlPersistMPool pool $ do
  users <- selectList [] []
  return $ entityVal <$> users

getProfiles :: ConnectionPool -> IO [Profile]
getProfiles pool = flip runSqlPersistMPool pool $ do
  profiles <- selectList [] []
  return $ entityVal <$> profiles

server :: ConnectionPool -> Server UserAPI
server pool = (liftIO $ getUsers pool) :<|> (liftIO $ getProfiles pool)

userApi :: Proxy UserAPI
userApi = Proxy

app :: ConnectionPool -> Application
app pool = serve userApi $ server pool

mkApp :: FilePath -> IO Application
mkApp sqliteFile = do
  pool <- runStderrLoggingT $ do
    createSqlitePool (cs sqliteFile) 5
  runSqlPool (runMigration migrateAll) pool
  runSqlPool (insert $ User $ UserUUID UUID.nil) pool
  return $ app pool
